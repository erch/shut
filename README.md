# sh utilities

A collection of bash functions.

## Content

### source_dir()

source all files that are readable in the directory given as parameter in alphanumeric order

### source_file()

source a file but check before that it has not been already sourced

### backupf()
create a copy of a file given as parameter with an .back_N extension where N represents the Nth backup of the file

### replace_in_file()

replace something by something else in a file or several files in a directory tree.

### insert_bloc_infile()

Insert or replace a block of text in file

### fill_template()

fill a mustache template. based on : https://github.com/rcrowley/mustache.sh

### pathmunge()

Add directory to the PATH environment variable and remove duplicates.


## Usage

dot source the shut.sh file with its own directory as parameter and use the function as you wish.
```bash
# get utility functions, gives also configuration directory location
if [[ -r $HOME/bin/shut.sh ]] ; then
    . $HOME/bin/shut.sh $HOME/bin
else
    exit 1
fi
...

```