set -e

# Source the shut funcs
. "../shut.sh" ..

# Set some variables.  They don't need to be exported to be made available
# to the `mustache` function.
FOO="foo" BAR="bar"

# Call the `fill_template` function, passing a template on standard input and
# diffing standard output against a knownZ-good copy.
find "fill-template-tests" -type f -name "*.mustache" | while read PATHNAME
do
	echo "$PATHNAME" >&2
	fill_template <"$PATHNAME" | diff -u - "$PATHNAME.out"
done
