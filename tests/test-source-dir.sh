# Source the tester
. ./tester.sh

# Source the shut functions
. "../shut.sh" ..

set -x
TEST_DIR=/tmp/test-source-dir$$
mkdir -p ${TEST_DIR}
PATH_ORIGIN=$PATH

function create_source_file() {
    local DIR=$1
    local PREF=$2
    local EXT=$4
    I=1
    for FILE_NAME in $3; do
        echo "echo -e $I \\\c" > ${DIR}/${PREF}${FILE_NAME}${EXT}
        (( I++ ))
    done
}

function test() {
    local NAMES=$1
    local EXT=$2
    local SUB_DIR=${TEST_DIR}/sources
    local PREF=testinputs_
    mkdir -p ${SUB_DIR}
    create_source_file "${SUB_DIR}" "${PREF}" "$NAMES" $EXT
    source_dir $SUB_DIR $EXT
    rm -f ${SUB_DIR}/${PREF}*
}

base_test() {
    cat > ${TEST_DIR}/base_test.exp <<-EOF
1 2 3 4
EOF

    test "a b c d" > ${TEST_DIR}/base_test.res
}

unordered_test() {
    cat > ${TEST_DIR}/unordered_test.exp <<-EOF
2 4 1 3
EOF

    test "c a d b" > ${TEST_DIR}/unordered_test.res
}

numerical_test() {
    cat > ${TEST_DIR}/numerical_test.exp <<-EOF
2 5 4 1 3
EOF

    test "a 0a b 2b 0c" > ${TEST_DIR}/numerical_test.res
}

double_test() {
    EXT="sh"
    SUB_DIR="${TEST_DIR}/dir"
    TEST_FILE="${SUB_DIR}/test.$EXT"
    mkdir -p "${SUB_DIR}"
    declare -A done
    cat > ${TEST_DIR}/double_test.exp <<-EOF
1
EOF
    echo "echo -e 1 \\\c" > ${TEST_FILE}
    source_dir $SUB_DIR $EXT done  > ${TEST_DIR}/double_test.res
    source_dir $SUB_DIR $EXT done >> ${TEST_DIR}/double_test.res
}

if [[ $# -gt 0 ]] ; then
    TEST_LIST=$@
else
    TEST_LIST="\
        base_test \
        unordered_test \
        numerical_test \
        double_test"
fi

run_test
