# Source the tester
. ./tester.sh

# Source the shut functions
. "../shut.sh"

TEST_DIR=/tmp/test-replace-in-file$$
mkdir -p ${TEST_DIR}

basic_test() {

    cat > ${TEST_DIR}/basic_test.res <<-EOF
foo
bar
foo
bar
EOF

    cat > ${TEST_DIR}/basic_test.exp <<-EOF
foo
foo
foo
foo
EOF

    replace_in_file \
            ${TEST_DIR}/basic_test.res \
            - \
            "bar" \
            "foo\n"
}

regex_test() {

    cat > ${TEST_DIR}/regex_test.res <<-EOF
foo
bar
foo
bar
EOF

    cat > ${TEST_DIR}/regex_test.exp <<-EOF
foo
fao
foo
fao
EOF

    replace_in_file \
            ${TEST_DIR}/regex_test.res \
            - \
            "b(a)r" \
            'f$1o\n'
}

quote_test() {

    cat > ${TEST_DIR}/quote_test.res <<-EOF
"foo"
"bar"
"foo"
"bar"
EOF

    cat > ${TEST_DIR}/quote_test.exp <<-EOF
"foo"
"foo"
"foo"
"foo"
EOF

    replace_in_file \
            ${TEST_DIR}/quote_test.res \
            - \
            '"bar"' \
            '"foo"\n'
}

space_test() {

    cat > ${TEST_DIR}/space_test.res <<-EOF
foo bar
bar
foo bar2
bar
EOF

    cat > ${TEST_DIR}/space_test.exp <<-EOF
foo bar
foo bar 3
foo bar2
foo bar 3
EOF

    replace_in_file \
            ${TEST_DIR}/space_test.res \
            - \
            "^\s*bar$" \
            "foo bar 3\n"
}

no_new_line_test() {

    cat > ${TEST_DIR}/no_new_line_test.res <<-EOF
foo
bar
EOF

    echo -e 'bar2\c' >> ${TEST_DIR}/no_new_line_test.res

    cat > ${TEST_DIR}/no_new_line_test.exp <<-EOF
foo
bar
bar2
foo2
foo3
EOF

    replace_in_file \
            ${TEST_DIR}/no_new_line_test.res \
            - \
            "toto" \
            "foo2" \
            1

    replace_in_file \
            ${TEST_DIR}/no_new_line_test.res \
            - \
            "titi" \
            "foo3" \
            1
}

if [[ $# -gt 0 ]] ; then
    TEST_LIST=$@
else
    TEST_LIST="\
        basic_test \
        regex_test \
        quote_test \
        space_test \
        no_new_line_test"
fi

run_test
