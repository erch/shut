
function run_test() {
    local FOUND_NOK=0
    local TEST_IN
    local TEST
    local LOG="${TEST_DIR}/log.txt"
    for TEST in $TEST_LIST ; do
        echo -e "$TEST : \c"
        exec 6>&1
        exec 5>&2
        exec > "${LOG}" 2>&1

        set -x
        $TEST
        set +x
        exec 1>&6 6>&-
        exec 2>&5 5>&-
        RES=$(diff -u -w ${TEST_DIR}/${TEST}.res "${TEST_DIR}/${TEST}.exp" 2>&1)
        if [[ $? -ne 0 ]]; then
            echo -e "NOK\n----------------------\n$RES\n=================\n"
            cat "${LOG}"
            echo "========================"
            (( FOUND_NOK++ ))
        else
            echo -e 'ok \n'
            rm -f "${TEST_DIR}/${TEST}.res" "${TEST_DIR}/${TEST}.exp"
        fi
        rm ${LOG}
    done
    if [[ "${FOUND_NOK}" -eq 0 ]] ; then
        (
            cd $(dirname ${TEST_DIR})
            rm -rf ${TEST_DIR}
        )
        exit 0
    else
        echo -e "\n${FOUND_NOK} Errors found, use:\n\trm -rf ${TEST_DIR}\nto clean directory"
        exit 1
    fi
}
