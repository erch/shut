# Source the tester
. ./tester.sh

# Source the shut functions
. "../shut.sh" ..

TEST_DIR=/tmp/test-path-munge$$
mkdir -p ${TEST_DIR}
PATH_ORIGIN=$PATH

function test() {
    local PATH=$1
    shift
    pathmunge $@
    echo $PATH
    PATH=$PATH_ORIGIN
}

clean_test() {
    cat > ${TEST_DIR}/clean_test.exp <<-EOF
/etc/bin:/etc/bina:/usr/bin
EOF

    test "/etc/bin:/etc/bina:/etc/bin:/usr/bin" > ${TEST_DIR}/clean_test.res
}

addbefore_test() {
    cat > ${TEST_DIR}/addbefore_test.exp <<-EOF
/toto/tata:/etc/bin:/etc/bina:/usr/bin
EOF

    test "/etc/bin:/etc/bina:/etc/bin:/usr/bin" "/toto/tata" > ${TEST_DIR}/addbefore_test.res
}

addafter_test() {
    cat > ${TEST_DIR}/addafter_test.exp <<-EOF
/etc/bin:/etc/bina:/usr/bin:/toto/tata
EOF

    test "/etc/bin:/etc/bina:/etc/bin:/usr/bin" "/toto/tata" after > ${TEST_DIR}/addafter_test.res
}

if [[ $# -gt 0 ]] ; then
    TEST_LIST=$@
else
    TEST_LIST="\
        clean_test \
        addbefore_test \
        addafter_test"
fi

run_test
